import React from 'react'
import { 
  StyleSheet, 
  Text, 
  View, 
  TouchableOpacity, 
  ScrollView,
  Dimensions,
  Image,
  TextInput
} from 'react-native'

// Images
const addPet = require('../assets/addPet.png')


export default class RegisterPetButton extends React.Component {
  render() {
    return (
      <TouchableOpacity
        style={styles.container}
        onPress={this.props.onPress}
      >
        <Image
            source={addPet}
            style={styles.image}
        />
        <View
            style={styles.textView}
        >
          <Text 
              style={styles.name}
          >
              Register a new pet
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: '#F77EBE',
    width: '90%',
    height: 60,
    borderRadius: 15,
    alignSelf: 'center',
    marginVertical: 5
  },
  textView:{
    marginLeft: '5%'
  },  
  name:{
    fontSize: 16,
    fontWeight: '700',
    color: '#FFF'
  },
  row:{
    flexDirection: 'row'
  },
  gender:{
    height: 17,
    width: 17,
  },    
  image:{
    height: 80,
    width: 100,
    borderRadius: 22,
  }
});
