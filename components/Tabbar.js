import React from 'react'
import { 
  StyleSheet, 
  Text, 
  View, 
  TouchableOpacity, 
  ScrollView,
  Dimensions,
  Image,
  TextInput
} from 'react-native'

const list = require('../assets/list.png')
const list_selected = require('../assets/list_selected.png')
const map = require('../assets/map.png')
const map_selected = require('../assets/map_selected.png')

export default class Tabbar extends React.Component {
  render() {
    return (
      <View
        style={styles.container}
      >
      <TouchableOpacity
        onPress={this.props.listPressed}
        styles={styles.tabButton}        
      >
          <Image
              source={this.props.listSelected == true ? list_selected : list}
              style={styles.icon}
          />
      </TouchableOpacity>

      <TouchableOpacity
        onPress={ this.props.mapPressed}
        style={styles.tabButton}
      >
          <Image
              source={this.props.listSelected == false ? map_selected : map}
              style={styles.icon}
          />
      </TouchableOpacity>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    bottom: 0,
    flexDirection: 'row',
    width: '100%',
    backgroundColor: '#EEF0FC',
    paddingVertical: '3%',
    justifyContent: 'space-around'
  },
  tabButton:{
        
  },
  icon:{
    height: 40,
    width: 40,
    alignSelf: 'center'
  },    
});
