import React from 'react'
import { 
  StyleSheet, 
  Text, 
  View, 
  TouchableOpacity, 
  ScrollView,
  Dimensions,
  Image,
  TextInput
} from 'react-native'

// Images
const male = require('../assets/male.png')
const female = require('../assets/female.png')

export default class PetListItem extends React.Component {
  render() {
    return (
      <TouchableOpacity
        style={styles.container}
        onPress={this.props.onPress}
      >
        <Image
            source={this.props.image}
            
            style={styles.image}
        />
        <View
            style={styles.textView}
        >
            <View
                style={styles.row}
            >
                <Text 
                    style={styles.name}
                >
                    {this.props.name}
                </Text>
                <Image
                    source={this.props.gender == "M" ? male : female}
                    style={styles.gender}
                />
            </View>
            <Text>{this.props.breed}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
      flexDirection: 'row',
      alignItems: 'center',
      backgroundColor: '#EEF0FC',
      width: '90%',
      height: 90,
      borderRadius: 22,
      alignSelf: 'center',
      marginVertical: 5
  },
  textView:{
    marginLeft: '5%'
  },  
  name:{
    fontSize: 16,
    fontWeight: '700'
  },
  row:{
    flexDirection: 'row'
  },
  gender:{
    height: 17,
    width: 17,
    marginLeft: '5%'
  },    
  image:{
      height: 80 ,
      width: 100,
      borderRadius: 22,
      marginLeft: 5,
  }
});
