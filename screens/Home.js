import React from 'react';
import {
  Alert,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TextInput
} from 'react-native';

// Third Party
import MapView from 'react-native-maps';
import Modal from 'react-native-modal';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker'; // Migration from 2.x.x to 3.x.x => showImagePicker API is removed.

// Custom Components
import PetListItem from '../components/PetListItem'
import Tabbar from '../components/Tabbar'
import RegisterPetButton from '../components/RegisterPetButton';

// Images
const logo = require('../assets/pet_pursuit.png')
const placeholder = require('../assets/hugo.png')
const bandit = require('../assets/bandit.jpg')
const starfox = require('../assets/starfox.jpg')
const sprinkles = require('../assets/placeholder.jpg')
const darkGrayCat = require('../assets/darkGrayCat.jpg')
const notMesa = require('../assets/fakeMesa.jpg')
const notCasper = require('../assets/fakeCasper.jpg')
const phone = require('../assets/phone.png')
const collar = require('../assets/dog-collar.png')
const turtle = require('../assets/turtle.png')
const dog = require('../assets/dog.png')
const cat = require('../assets/cat.png')
const male = require('../assets/male.png')
const female= require('../assets/female.png')
const image = require('../assets/image.png')
const success = require('../assets/success.png')


// This page is the landing page for the application, it is a very large file
// that should honestly be split into even more components. It houses the list,
// map and regestration modal and all of the logic that goes with hiding and showing
// all of those pieces.

// Dummy Data to seed the database, these are used solely for the purpose of demonstration
let testData = [
    {
        "id" : 1,
        "name": "Hugo",
        "type": "dog",
        "breed": "Border Collie/Blue Healer",
        "gender": "M",
        "description": "Not the brightest but is a very good boy. He is also quite needy so I would make sure that you can dedicate plenty of time to petting him if you do not enjoy the sound of whining. He knows how to shake hands and listens very well when told to lay down. Might howl sometimes at 3 AM but that is part of his charm.",
        "lat": 37.14227270909956,
        "lon": -93.26040309954308,
        "age": 6,
        "contactNumber" : '417-777-7777',
        "image" : placeholder
    },
    {
        "id" : 2,
        "name": "Mesa",
        "type": "dog",
        "breed": "Border Collie/Blue Healer",
        "gender": "F",
        "description": "A very sweet dog that is generally very tame, she does really like to play fetch. She is very smart and can be very loving. Would be a great addition to any familes home!",
        "lat": 37.180301647235,
        "lon": -93.296971397606,
        "age": 6,
        "contactNumber" : '417-777-7777',
        "image" : notMesa
    },
    {
        "id" : 3,
        "name": "Starfox",
        "type": "cat",
        "breed": "Manx",
        "gender": "M",
        "description": "One of the sweetest cats, will lay in the sun most of the day but he will also peridoically come make sure you are giving him enough attention. Loves being around people, whether he knows them well or not. Does sometimes like to play a little rough and could accidently scratch you. Could ask for nothing more, given his age!",
        "lat": 37.20022807156659,
        "lon": -93.28197538510572,
        "age": 18,
        "contactNumber" : '417-777-7777',
        "image" : starfox
    },
    {
        "id" : 4,
        "name": "Bandit",
        "type": "cat",
        "breed": "Himalyan",
        "gender": "M",
        "description": "The cat is very loud and demands a decent amount of attention. Does not love the introduction of new people into the home, he will tend to hide. He also really likes being in small spaces, so you may find him most of the time hiding under couches/beds more often than not.",
        "lat": 37.134762293691665,
        "lon": -93.28146551786203,
        "age": 16,
        "contactNumber" : '417-777-7777',
        "image" : bandit
    },
    {
        "id" : 5,
        "name": "Boo",
        "type": "cat",
        "breed": "Charteux",
        "gender": "M",
        "description": "This cat is very scared of almost everything, does not get along great with other animals either. Would prefer a more quiet home but does not require much attention.",
        "lat": 37.16153743368484,
        "lon": -93.26521173100815,
        "age": 4,
        "contactNumber" : '417-777-7777',
        "image" : darkGrayCat
    },
    {
        "id" : 7,
        "name": "Sprinkles",
        "type": "dog",
        "breed": "German Shepard",
        "gender": "M",
        "description": "The best farm dog a man could ask for!",
        "lat":  37.14529567908677,
        "lon": -93.32320457556172,
        "age": 1,
        "contactNumber" : '417-777-7777',
        "image" : sprinkles
    },
    {
        "id" : 8,
        "name": "Casper",
        "type": "cat",
        "breed": "Khao Manee",
        "gender": "M",
        "description": "The friendliest of ghosts",
        "lat": 37.16153743368484,
        "lon": -93.26521173100815,
        "age": 1,
        "contactNumber" : '417-777-7777',
        "image" : notCasper
    },
]


export default class Home extends React.Component {
    state = {
        listSelected: true,
        addPetModalOpen: false,
        showFirst: true,
        registerName: '',
        registerAge: 0,
        dogSelected: false,
        catSelected: false,
        maleSelected: false,
        femaleSelected: false,
        photo: '',
        showPhoto: false,
    }

    // Updates the view on home to display either the list or the map depending
    // what button in the tab bar the user presses
    listPressed(){
        this.setState({
            listSelected: true
        })
    }
    mapPressed(){
        this.setState({
            listSelected: false
        })
    }

    // Opens the pet registration modal over the home view
    openAddPetModal(){
        this.setState({
            addPetModalOpen: true
        })
    }

    // Resets the state of the form back to empty fields and
    // shows the first page of the modal
    closeModal(){
        this.setState({
            addPetModalOpen: false,
            showFirst: true,
            showSecond: false,
            showThird: false,
            showSuccess: false,
            registerName: '',
            registerAge: 0,
            dogSelected: false,
            catSelected: false,
            maleSelected: false,
            femaleSelected: false,
            photo: '',
            showPhoto: false,
        })
    }

    // Checks what page the user is currently on and displays the next
    // page of the modal. No parameters are required as we are using the
    // classes global state
    formSubmit(){
        if(this.state.showFirst == true){
            this.setState({
                showFirst: false,
                showSecond: true
            })
        }else if(this.state.showSecond == true)
        {
            this.setState({
                showSecond: false,
                showThird: true
            })
        }else if(this.state.showThird == true)
        {
            this.setState({
                showThird: false,
                showSuccess: true
            })
        }
    }

render(){
  // Map object of all of the pets in out list component
  const PetList = () => {
    return testData.map(pet => {
        return(
            <PetListItem
                id={pet.id}
                key={pet.id}
                name={pet.name}
                breed={pet.breed}
                image={pet.image}
                gender={pet.gender}
                pet={pet}
                onPress={()=>this.props.navigation.navigate('Pet',{
                    petName: pet.name,
                    breed: pet.breed,
                    image: pet.image,
                    gender: pet.gender,
                    age: pet.age,
                    description: pet.description,
                    pet: pet
                })}
            />
        )
    })
  }

// Uses our image picker library to launch the camera 
// and return the image taken by the device to the view to be displayed
// in the modal
openPicker = () => {
    let options = {
        title: "Pick a clear photo to show off " + this.state.petName + "!",
        storageOptions: {
          skipBackup: true,
          path: 'images',
        },
      }
    launchCamera(options, (response) => { // Use launchImageLibrary to open image gallery
      console.log('Response = ', response);
    
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };
        this.setState({
            photo: response.uri,
            showPhoto: true
        })
      }
    });
}

  return (
    <View
        style={styles.container}
    >
      <Image
        source={logo}
        style={styles.logo}
      />
     
      { this.state.listSelected &&
        <ScrollView
            scrollEnabled={true}
            style={styles.scroll}
        >
            <RegisterPetButton
                onPress={()=>this.openAddPetModal()}
            />
            <PetList/>
        </ScrollView>
      }
      { this.state.listSelected == false &&
            <View
                style={{borderRadius: 50, overflow: 'hidden'}}
            >
                <MapView
                    style={styles.mapView}
                    initialRegion={{
                        latitude: 37.210388,
                        longitude: -93.2923,
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421,
                    }}
                >
                    { testData.map((pet, index) => (
                        <MapView.Marker
                            key={index}
                            coordinate={{latitude: pet.lat, longitude: pet.lon}}
                            title={pet.name}
                            description={pet.breed}
                            pet={pet}
                            pinColor={'#F77EBE'}
                            onCalloutPress={()=>this.props.navigation.navigate('Pet',{
                                petName: pet.name,
                                breed: pet.breed,
                                image: pet.image,
                                gender: pet.gender,
                                age: pet.age,
                                description: pet.description,
                                pet: pet
                            })}
                        />
                    ))}
                </MapView>
            </View>
       }
      <Tabbar
        listPressed={() => this.listPressed()}
        mapPressed={() => this.mapPressed()}
        listSelected={this.state.listSelected}
      />
      <Modal 
        isVisible={this.state.addPetModalOpen}
        statusBarTranslucent
      >
            <View 
                onPress={()=>this.closeModal()}
                style={{
                    flex:1,
                    alignContent: 'center',
                    justifyContent: 'center'
                }}>
                   
                    <View
                        style={{
                            backgroundColor: '#FFF',
                            height: '80%',
                            width: '100%',
                            alignSelf: 'center',
                            borderRadius: 15,
                            alignContent: 'center',
                            overflow: 'hidden'
                        }} 
                    >
                        <View
                            style={{
                                justifyContent: 'space-between',
                                padding: '10%',
                            }}
                        >
                            { this.state.showFirst &&
                            <View>
                                <Text style={styles.registerTitle}>Pet Registration</Text>
                                <Text style={styles.registerSubTitle}>Thank you for choosing to re-home your pet on Pet Pursuit!</Text>                            
                                <View
                                    style={styles.inputRow}
                                >
                                    <Image
                                        source={phone}
                                        style={styles.inputIcon}
                                    />
                                    <TextInput
                                        placeholder="Phone number"
                                        style={styles.registerInput}
                                        onChangeText={(text)=>this.setState({registerAge: text})}
                                        val={this.state.registerAge}
                                        keyboardType="number-pad"
                                    />
                                </View>
                                <View
                                    style={styles.inputRow}
                                >
                                    <Image
                                        source={collar}
                                        style={styles.inputIcon}
                                    />
                                    <TextInput
                                        placeholder="Name"
                                        style={styles.registerInput}
                                        onChangeText={(text)=>this.setState({registerName: text})}
                                        val={this.state.registerName}
                                    />
                                </View>
                                <View
                                    style={styles.inputRow}
                                >
                                    <Image
                                        source={turtle}
                                        style={styles.inputIcon}
                                    />
                                    <TextInput
                                        placeholder="Age"
                                        style={styles.registerInput}
                                        onChangeText={(text)=>this.setState({registerAge: text})}
                                        val={this.state.registerAge}
                                        keyboardType="number-pad"
                                    />
                                </View>
                                <View
                                    style={styles.buttonRow}
                                >
                                    <TouchableOpacity
                                        style={[styles.largeButton, {backgroundColor: this.state.dogSelected ? '#F77EBE' : '#eee'}]}
                                        onPress={()=>this.setState({catSelected: false, dogSelected: true})}
                                    >
                                        <Image
                                            source={dog}
                                            resizeMode={'contain'}
                                            style={styles.largeButtonIcon}
                                        />
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        style={[styles.largeButton, {backgroundColor: this.state.catSelected ? '#F77EBE' : '#eee'}]}
                                        onPress={()=>this.setState({catSelected: true, dogSelected: false})}
                                    >
                                        <Image
                                            source={cat}
                                            resizeMode={'contain'}
                                            style={styles.largeButtonIcon}
                                        />
                                    </TouchableOpacity>
                                    </View>
                            </View>
                            }

                            {this.state.showSecond &&
                            <View>
                                <Text style={styles.registerTitle}>Can you tell us a little more about {this.state.registerName}?</Text>                            
                                <View
                                    style={styles.buttonRow}
                                >
                                    <TouchableOpacity
                                        style={[styles.largeButton, {backgroundColor: this.state.maleSelected ? '#F77EBE' : '#eee'}]}
                                        onPress={()=>this.setState({maleSelected: true, femaleSelected: false})}
                                    >
                                        <Image
                                            source={male}
                                            resizeMode={'contain'}
                                            style={styles.largeButtonIcon}
                                        />
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        style={[styles.largeButton, {backgroundColor: this.state.femaleSelected ? '#F77EBE' : '#eee'}]}
                                        onPress={()=>this.setState({maleSelected: false, femaleSelected: true})}
                                    >
                                        <Image
                                            source={female}
                                            resizeMode={'contain'}
                                            style={styles.largeButtonIcon}
                                        />
                                    </TouchableOpacity>
                                </View>
                                    <TextInput
                                        placeholder="Short Description"
                                        style={styles.registerTextAreaInput}
                                        onChangeText={(text)=>this.setState({registerDescription: text})}
                                        val={this.state.registerDescription}
                                        multiline={true}
                                    />
                            </View>
                            }
                            { this.state.showThird &&
                                <View>
                                    <Text style={styles.registerTitle}>Almost there, just show the world what {this.state.registerName} looks like!</Text>

                                    { this.state.showPhoto == false &&
                                        <TouchableOpacity
                                            onPress={openPicker}
                                            style={styles.imageButton}
                                        >
                                            <Image
                                                source={image}
                                                style={styles.imageIcon}
                                            />
                                            
                                        </TouchableOpacity>
                                    }

                                    {this.state.showPhoto == true &&
                                    <TouchableOpacity
                                        onPress={openPicker} 
                                    >
                                            <Image
                                                source={{uri: this.state.photo} }
                                                style={styles.touchableImage}
                                            />
                                    </TouchableOpacity>
                                    }
                                </View>
                            }

                            {this.state.showSuccess &&
                            <View>
                                <Image
                                    source={success}
                                    resizeMode={'contain'}
                                    style={styles.successImage}
                                />
                                <Text 
                                    style={[styles.registerTitle, {alignSelf: 'center'}]}
                                >
                                    The Pet Pursuit team is reviewing your posting!
                                </Text>
                                <Text
                                    style={[styles.registerSubTitle, {alignSelf: 'center', marginTop: 15}]}

                                >
                                We are excited to have {this.state.registerName} and are confident {this.state.maleSelected ? 'he ' : 'she '}
                                will have a new home soon!
                                </Text>
                            </View>
                            }
                        </View>

                        { !this.state.showSuccess &&
                        <View
                            style={{
                                position: 'absolute',
                                bottom: 12,
                                flexDirection: 'row',
                                width: '100%',
                                height: 70,
                                paddingVertical: '3%',
                                justifyContent: 'space-evenly'
                            }}
                        >
                            <TouchableOpacity
                                style={styles.cancelRegisterButton}
                                onPress={()=>this.closeModal()}
                            >
                                <Text style={styles.pickupText}>Cancel</Text>
                            </TouchableOpacity>
                            
                            <TouchableOpacity
                                onPress={()=> this.formSubmit()}
                                style={styles.registerButton}
                            >
                                <Text style={styles.next}>Next</Text>
                            </TouchableOpacity>
                        </View>
                        }
                        {this.state.showSuccess &&
                        <View
                            style={{
                                position: 'absolute',
                                bottom: 12,
                                flexDirection: 'row',
                                width: '100%',
                                height: 70,
                                paddingVertical: '3%',
                            }}
                        >      
                            <TouchableOpacity
                                onPress={()=> this.closeModal()}
                                style={[styles.registerButton, {width: '100%'}]}
                            >
                                <Text style={styles.next}>Done</Text>
                            </TouchableOpacity>
                        </View>
                        }
                    </View>
                </View>
            </Modal>
    </View>
        );
    };
};

const styles = StyleSheet.create({
    container:{
        backgroundColor: "#FFF",
        flex:1,
    },
    scroll:{
        marginBottom: '17%'
    },
    mapView:{
        height: '100%',
        width: '100%',
        alignSelf: 'center',
        borderRadius: 30,
        overflow: 'hidden' 
    },
    logo:{
        height: 75,
        width: 170,
    },
    registerInput:{
        width: '90%',
        backgroundColor: '#eee',
        borderRadius: 12,
        padding: 10,
    },
    
    addButton:{
        alignSelf: 'center',
        backgroundColor: '#F77EBE',
        width: '100%',
        alignContent: 'center',
        alignItems: 'center',
        height: '8%',
        borderRadius: 0,
        justifyContent: 'center',  
    },
    addButtonText:{
        color: '#FFF',
        fontSize: 17,
    },
    registerTitle: {
      fontSize: 24,
    }, 
    inputRow:{
        flexDirection: 'row',
        width: '100%',
        marginTop: 35,
    },
    buttonRow:{
        flexDirection: 'row',
        height: 200,
        width: '100%',
        marginTop: 35,
        justifyContent: 'space-between'
    },
    registerTextAreaInput: {
        width: '100%',
        backgroundColor: '#eee',
        borderRadius: 12,
        padding: 10,
        height: '45%',
        textAlignVertical: 'top',
        marginTop: '-25%',
    },
    inputIcon:{
        height: 25,
        width: 25,
        alignSelf: 'center',
        marginRight: '5%',
    },
    largeButton:{
        height: '40%',
        width: '40%',
        backgroundColor: '#eee',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 12,
    },
    largeButtonIcon:{
        height: '40%',
        width: '50%'
    },  
    imageButton:{
        height: 350,
        width: 280,
        backgroundColor: '#eee',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        borderRadius: 25,
        marginTop: 10,
    },
    imageIcon:{
        height: 100,
        width: 100,
    },  
    touchableImage:{
        height: 350,
        width: 280,
        backgroundColor: '#eee',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        borderRadius: 25,
        marginTop: 10,
    },  
    cancelRegisterButton: {
        flexDirection: 'column',
        marginTop: '7%',
        backgroundColor: '#EEF0FC',
        width: '50%',
        alignContent: 'flex-end',
        alignItems: 'center',
        height: "100%",
        justifyContent: 'center', 
    }, 
    registerButton: {
        flexDirection: 'column',
        marginTop: '7%',
        backgroundColor: '#F77EBE',
        width: '50%',
        alignContent: 'flex-end',
        alignItems: 'center',
        height: "100%",
        justifyContent: 'center', 
    },
    next:{
        color: "#FFF"
    },
    successImage:{
        height: '75%',
        width: '80%',
        alignSelf: 'center'
    }
});

