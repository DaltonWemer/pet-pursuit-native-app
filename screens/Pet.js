

import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity
} from 'react-native';

// Third party libraries
import ConfettiCannon from 'react-native-confetti-cannon';
import Modal from 'react-native-modal';
import Communications from 'react-native-communications';

// Images
const male = require('../assets/male.png')
const female = require('../assets/female.png')
const messageIcon = require('../assets/messenger.png')

// This page can only be accessed by Home.js, threfore it relies on the data
// being passed in the navigator to render. This view represents a more detailed
// look at the pet you might be interested in adopting

export default class Pet extends React.Component {
    state = {
        adopted: false
    }

    // Triggers the confetti and adopted modal to reach out and text
    // the poster
    adopt(){
        this.setState({
            adopted: true
        })
    }

render(){
  return (
    <SafeAreaView
        style={styles.container}
    >
        <StatusBar
            backgroundColor="#FFF"          
            hidden={true} 
            translucent={true}
          />
        <Image
            source={this.props.route.params.image}
            style={styles.photo}
        />
        <View
            style={styles.textView}
        >
            <View
                style={styles.row}
            >
                <Text style={styles.name}>{this.props.route.params.petName}</Text>
                <Image
                    source={this.props.route.params.gender == "M" ? male : female}
                    style={styles.genderIcon}
                />
            </View>
            <Text>{this.props.route.params.breed}</Text>
            <Text>{"Age: " + this.props.route.params.age}</Text>
            <ScrollView
                style={{
                    height: '30%'
                }}
            >
                <Text
                    style={styles.description}
                >
                    {this.props.route.params.description}
                </Text>
            </ScrollView>
            
          

            <Modal isVisible={this.state.adopted}>
                <View style={{
                    flex:1,
                    alignContent: 'center',
                    justifyContent: 'center'
                }}>
                    <ConfettiCannon count={200} origin={{x: 0, y: 0}}fadeOut={true} />
                    <View
                        style={{
                            backgroundColor: '#FFF',
                            height: '40%',
                            width: '100%',
                            alignSelf: 'center',
                            borderRadius: 15,
                            alignContent: 'center'
                        }}
                    >
                        <View
                            style={{
                                justifyContent: 'space-between',
                                padding: '10%',
                            }}
                        >
                            <Text style={styles.congratsText}>Congrats on giving {this.props.route.params.petName} a new home!</Text>
                            <Text>The current owners have been notified and are excited to hear from you soon! </Text>
                        </View>
                        <TouchableOpacity
                            onPress={() => Communications.text(this.props.route.params.pet.contactNumber)}
                            style={styles.messageButton}
                        >
                            <Image
                                source={messageIcon}
                                resizeMode={'contain'}
                                style={styles.messageIcon}
                            />
                            <Text style={styles.pickupText}>Setup a pickup time!</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={()=>this.setState({adopted: false})}
                            style={styles.cancelButton}
                        >
                            <Text>Maybe Later</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
            <TouchableOpacity
                onPress={()=>this.adopt()}
                style={styles.adoptButton}
            >
                <Text style={styles.adoptText}>Adopt</Text>
            </TouchableOpacity>
        </View>
    </SafeAreaView>
        );
    };
};

const styles = StyleSheet.create({
    container:{
        backgroundColor: "#FFF",
        flex:1,
    },
    textView:{
        padding: 10,
        borderRadius: 20,
        zIndex: 20,
        marginTop: '-7%',
        backgroundColor: 'white',
    },
    name:{
        fontSize: 40
    },  
    photo:{
        height: '50%',
        width: '100%',
    },
    row:{
        flexDirection: 'row',
    }, 
    congrats:{
        height: '80%',
        width: '80%',
        color: '#FFF',
        shadowColor: 'gray',
        shadowOpacity: 0.3,
    },  
    genderIcon:{
        height: 30,
        width: 30,
        marginLeft: 10,
        marginTop: 15,
    },  
    adoptButton:{
        alignSelf: 'center',
        backgroundColor: '#F77EBE',
        width: '90%',
        alignContent: 'center',
        alignItems: 'center',
        height: '13%',
        borderRadius: 15,
        justifyContent: 'center',
        marginTop: '-10%',
    },
    messageButton:{
        flexDirection: 'row',
        alignSelf: 'center',
        marginTop: '7%',
        backgroundColor: '#F77EBE',
        width: '90%',
        alignContent: 'center',
        alignItems: 'center',
        height: '13%',
        borderRadius: 10,
        justifyContent: 'center',  
    },
    cancelButton:{
        flexDirection: 'row',
        alignSelf: 'center',
        marginTop: '1%',
        backgroundColor: '#eee',
        width: '90%',
        alignContent: 'center',
        alignItems: 'center',
        height: '13%',
        borderRadius: 10,
        justifyContent: 'center',  
    },
    congratsText:{
        fontSize: 24,
    },  
    adoptText:{
        color: '#FFF',
        fontSize: 17,
    },
    pickupText:{
        color: '#FFF'
    },  
    messageIcon:{
        height: 19,
        width: 19,
        marginRight: 5,
    },  
    description:{
        marginTop: '10%'
    },
});

