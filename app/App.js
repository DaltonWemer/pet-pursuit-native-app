import React from 'react'
import { 
  StatusBar
} from 'react-native'

// Screens for the Navigator
import  Home  from '../screens/Home'
import  Pet  from '../screens/Pet'

// Third Party Packages
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

// The application's core navigator, this renders both of the required views
// and allows us to navigate to them from anywhere in the app.
// To add new pages to the application in the future, all we will need to do
// is import the page above and add it into our stack navigator

export default class App extends React.Component {  
  render() {
      const Stack = createStackNavigator();
      return (
        <NavigationContainer>
          <StatusBar
            backgroundColor="#FFF"          
            hidden={false} 
          />
          <Stack.Navigator
              screenOptions={{
                  headerShown: false
              }}
          >
              <Stack.Screen name="Home" component={Home} />
              <Stack.Screen name="Pet" component={Pet} />
            </Stack.Navigator>
        </NavigationContainer>
      ); 
  }

}

